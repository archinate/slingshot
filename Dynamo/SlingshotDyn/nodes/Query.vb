﻿Imports System
Imports System.IO
Imports System.Data

Public Module Query

#Region "Node Functions"

    ''' <summary>
    ''' ODBC Query
    ''' </summary>
    ''' <param name="ConnectionString">The connection string</param>
    ''' <param name="Toggle">Toggle the connection on/off</param>
    ''' <param name="Query">The SQL Query</param>
    ''' <returns>Query result</returns>
    ''' <search>interoperability, database, slingshot, query</search>
    Public Function ODBC_Query(ConnectionString As String,
                                      Toggle As Boolean,
                                      Query As String) As Object()()
        Dim m_SqlDataSet As DataSet = Nothing
        If Toggle = True Then
            ' Get DataSet
            Dim m_dbcmd As New clsRDBMS()
            m_SqlDataSet = m_dbcmd.ODBCQuery(ConnectionString, Query)
        End If

        Dim ds As DataSet = m_SqlDataSet
        Dim m_dt As DataTable = ds.Tables(0)

        Dim m_rowscount As Integer = m_dt.Rows.Count
        Dim m_colscount As Integer = m_dt.Columns.Count

        ' Parse DataSet
        Dim m_outarr As Object()() = New Object(m_rowscount - 1)() {}

        For i As Integer = 0 To m_rowscount - 1
            m_outarr(i) = New Object(m_colscount - 1) {}
            For j As Integer = 0 To m_colscount - 1
                m_outarr(i)(j) = m_dt.Rows(i)(j)
            Next
        Next

        Return m_outarr
    End Function

    ''' <summary>
    ''' ODBC Query
    ''' </summary>
    ''' <param name="ConnectionString">The connection string</param>
    ''' <param name="Toggle">Toggle the connection on/off</param>
    ''' <param name="Query">The SQL Query</param>
    ''' <returns>Query result</returns>
    ''' <search>interoperability, database, slingshot, query</search>
    Public Function OLEDB_Query(ConnectionString As String,
                                      Toggle As Boolean,
                                      Query As String) As Object()()
        Dim m_SqlDataSet As DataSet = Nothing
        If Toggle = True Then
            ' Get DataSet
            Dim m_dbcmd As New clsRDBMS()
            m_SqlDataSet = m_dbcmd.OLEDBQuery(ConnectionString, Query)
        End If

        Dim ds As DataSet = m_SqlDataSet
        Dim m_dt As DataTable = ds.Tables(0)

        Dim m_rowscount As Integer = m_dt.Rows.Count
        Dim m_colscount As Integer = m_dt.Columns.Count

        ' Parse DataSet
        Dim m_outarr As Object()() = New Object(m_rowscount - 1)() {}

        For i As Integer = 0 To m_rowscount - 1
            m_outarr(i) = New Object(m_colscount - 1) {}
            For j As Integer = 0 To m_colscount - 1
                m_outarr(i)(j) = m_dt.Rows(i)(j)
            Next
        Next

        Return m_outarr
    End Function

    ''' <summary>
    ''' MySQL Query
    ''' </summary>
    ''' <param name="ConnectionString">The connection string</param>
    ''' <param name="Toggle">Toggle the connection on/off</param>
    ''' <param name="Query">The SQL Query</param>
    ''' <returns>Query result</returns>
    ''' <search>interoperability, database, slingshot, query</search>
    Public Function MySQL_Query(ConnectionString As String,
                                      Toggle As Boolean,
                                      Query As String) As Object()()
        Dim m_SqlDataSet As DataSet = Nothing
        If Toggle = True Then
            ' Get DataSet
            Dim m_dbcmd As New clsRDBMS()
            m_SqlDataSet = m_dbcmd.MySQLQuery(ConnectionString, Query)
        End If

        Dim ds As DataSet = m_SqlDataSet
        Dim m_dt As DataTable = ds.Tables(0)

        Dim m_rowscount As Integer = m_dt.Rows.Count
        Dim m_colscount As Integer = m_dt.Columns.Count

        ' Parse DataSet
        Dim m_outarr As Object()() = New Object(m_rowscount - 1)() {}

        For i As Integer = 0 To m_rowscount - 1
            m_outarr(i) = New Object(m_colscount - 1) {}
            For j As Integer = 0 To m_colscount - 1
                m_outarr(i)(j) = m_dt.Rows(i)(j)
            Next
        Next

        Return m_outarr
    End Function

    ''' <summary>
    ''' SQLite Query
    ''' </summary>
    ''' <param name="FilePath">The connection string</param>
    ''' <param name="Toggle">Toggle the connection on/off</param>
    ''' <param name="Query">A SQL Query</param>
    ''' <returns>Query result</returns>
    ''' <search>interoperability, database, slingshot, query</search>
    Public Function SQLite_Query(FilePath As String,
                                      Toggle As Boolean,
                                      Query As String) As Object()()
        Try
            Dim m_SqlDataSet As DataSet = Nothing
            If Toggle = True Then
                ' Get DataSet
                Dim m_dbcmd As New clsRDBMS()
                m_SqlDataSet = m_dbcmd.SQLiteQuery(FilePath, Query)
            End If

            Dim ds As DataSet = m_SqlDataSet
            Dim m_dt As DataTable = ds.Tables(0)

            Dim m_rowscount As Integer = m_dt.Rows.Count
            Dim m_colscount As Integer = m_dt.Columns.Count

            ' Parse DataSet
            Dim m_outarr As Object()() = New Object(m_rowscount - 1)() {}

            For i As Integer = 0 To m_rowscount - 1
                m_outarr(i) = New Object(m_colscount - 1) {}
                For j As Integer = 0 To m_colscount - 1
                    m_outarr(i)(j) = m_dt.Rows(i)(j)
                Next
            Next

            Return m_outarr
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

End Module
